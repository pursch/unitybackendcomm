using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/**************** the code below is of no use to you **********************************/
public class BackendRequestFailedArgs: EventArgs {
	public int reasonId;
	public string message;
	public BackendRequestFailedArgs(int reasonCode, string errorString) {
		this.reasonId = reasonCode;
		this.message = errorString;
	}
};
public class UserDetails: EventArgs {
	public int id = -1;
	public string name = "";
	public string email = "";
	public int houseId = -1;
	public int points = 0;
	public string passwordHashed = "";
};
public class HouseDetails: EventArgs {
	public int id = -1;
	public string name = "";
	public string description = "";
};

public class HouseListingDetails: EventArgs {
	public List<HouseDetails> list = new List<HouseDetails>();
};
public class UserListingDetails: EventArgs {
	public List<UserDetails> list = new List<UserDetails>();
};

public class Backend : MonoBehaviour {	
	public event onUserLoginSuccess OnUserLoginSuccess;
	public event onUserLoginFailed OnUserLoginFailed;
	public event onUserCreateSuccess OnUserCreateSuccess;
	public event onUserCreateFailed OnUserCreateFailed;
	public event onHouseCreateSuccess OnHouseCreateSuccess;
	public event onHouseCreateFailed OnHouseCreateFailed;
	public event onHouseFindSuccess OnHouseFindSuccess;
	public event onHouseFindFailed OnHouseFindFailed;
	public event onHouseJoinSuccess OnHouseJoinSuccess;
	public event onHouseJoinFailed OnHouseJoinFailed;
	public event onHouseListingReceived OnHouseListingReceived;
	public event onUserListingForHouseReceived OnUserListingForHouseReceived;
		
	public static int USER_CREATE_ERROR_NAMEEXISTS = 0;
	public static int USER_CREATE_ERROR_MAILEXISTS = 1;
	public enum Commands: int {
		UserLogin = 1,
		UserCreate = 2,
		HouseJoin = 3,
		// ...
		HouseCreate = 11,
		HouseFind = 12,
		HouseListAll = 13,
		HouseListUsers = 14,
		count
	};
	
	/** string username, 
	string password 
	bool passwordIsHashed (set to true if you are sending the password in hashed format) 
	triggers OnUserLoginSuccess event */
	public void userLogin(string username, string password, bool passwordIsHashed = false) {
		string hashedPw;
		if(passwordIsHashed) {
			hashedPw = "true";
		} else {
			hashedPw = "false";	
		}	
		string jsondata = "{\"username\":\""+ username +"\",\"password\":\"" + password + "\",\"hash\":"+hashedPw+"}";
		string url = "http://softalogic.nl/gl5/api.php?module=player&action=login&data=" + WWW.EscapeURL(jsondata);
		this.Get(url);
	}

/** string desired username
	string desired password // the server will send back a hash of your password, to be used for future logins
	string your email address 
	triggers OnUserCreateSuccess event */
public void userCreate(string username, string password, string email) {
	string jsondata = "{\"username\":\""+ username +"\",\"password\":\""+ password +"\",\"email\":\""+email+"\"}";
	string url = "http://softalogic.nl/gl5/api.php?module=player&action=create&data=" + WWW.EscapeURL(jsondata);
	this.Get(url);
	return;
}

/** int id of house to join 
	int id of user which is to join the house
	string hashedPassword (will likely be removed in the future) 
	triggers OnHouseJoinSuccess event */
	public void houseJoin(int houseId, int userId, string passwordHashed) {		
		string jsondata = "{\"house_id\":"+ houseId +",\"user_id\":"+ userId +",\"hash\":\""+ passwordHashed +"\"}";
		string url = "http://softalogic.nl/gl5/api.php?module=player&action=joinhouse&data=" + WWW.EscapeURL(jsondata);
		this.Get(url);
	}
	
/** string desired house name (you can search for houses using partial matches of this data)
	string desired house description (you can search for houses using partial matches of this data)
	int creator player id 
	triggers OnHouseCreateSuccess event */
	public void houseCreate(string displayName, string description, int ownerPlayerId) {
		string jsondata = "{\"name\":\""+ displayName +"\",\"description\":\""+ description +"\",\"playerid\":"+ ownerPlayerId +"}";
		string url = "http://softalogic.nl/gl5/api.php?module=house&action=create&data=" + WWW.EscapeURL(jsondata);
		this.Get(url);
	}
	
/** string partial or full name to look for
	string partial or full name to look for 
	triggers OnHouseFindSuccess event */	
	public void houseFind(string name, string description) {
		string jsondata = "{\"name\":\""+ name +"\",\"description\":\""+ description +"\"}";
		string url = "http://softalogic.nl/gl5/api.php?module=house&action=find&data=" + WWW.EscapeURL(jsondata);
		this.Get(url);
	}

/** just list all houses registered in the system (not very useful?) 
	triggers OnHouseListingReceived event */
	public void houseFetchListing() {
		string url = "http://softalogic.nl/gl5/api.php?module=house&action=list";
		this.Get(url);
	}
	
/** find all users that are registered to a house with id HouseId
	triggers OnUserListingForHouseReceived event */
	public void userFetchListingForHouseWithId(int houseId) {
		string jsondata = "{\"house_id\":"+ houseId +"}";
		string url = "http://softalogic.nl/gl5/api.php?module=house&action=getuserlist&data=" + WWW.EscapeURL(jsondata);
		Debug.Log(WWW.UnEscapeURL(url));
		this.Get(url);
	}
	/** nothing of value to you below this line *******************************************/
	public WWW Get(string url) {
		WWW www = new WWW(url);
		Debug.Log("GET-ing url: " + url);
		StartCoroutine(WaitForRequest(www));
		return www;
	}
	public WWW Post(string url, Dictionary<string, string> post) {
		Debug.Log("POST-ing url:" + url);
		WWWForm form = new WWWForm();
		foreach(KeyValuePair<string, string> post_arg in post) {
			form.AddField(post_arg.Key, post_arg.Value);
		}
		WWW www = new WWW(url, form);
		StartCoroutine(WaitForRequest(www));
		return www;
	}
	private IEnumerator WaitForRequest(WWW www) {
		yield return www;
		
		//check for errors
		if(www.error == null) {
			Debug.Log("www ok!: " + www.text);
			JSONObject json = new JSONObject(www.text);
//			accessData(json);
			int cid = -1;
			if(json.HasField("cid")) {
				cid = (int)json.GetField("cid").n;
			} else {
				if(json.type == JSONObject.Type.ARRAY) {
					JSONObject p = (JSONObject)json.list[ 0 ];
					if(p.type == JSONObject.Type.OBJECT) {
						if(p.HasField("cid")) {
							cid = (int)p.GetField("cid").n;	
						} else {
							Debug.Log("response without commandId received and discarded");
							yield break;	
						}
					} else {
						Debug.Log("response without commandId received and discarded");
						yield break;	
					}
				} else {
					Debug.Log("response without commandId received and discarded");
					yield break;	
				}
			}
			switch(cid) {
				case (int)Commands.UserCreate: {
					if(json.HasField("player_email")) {
						UserDetails user = new UserDetails();
						user.id = Convert.ToInt32(json.GetField("player_id").str);
						user.email = json.GetField("player_email").str;
						user.passwordHashed = json.GetField("player_password").str;
						user.name = json.GetField ("player_name").str;
						this.OnUserCreateSuccess(this, user);
					} else {
						this.OnUserCreateFailed(this, new BackendRequestFailedArgs(-1, "User Create Failed: player name or email already in use"));
					}	
					break;
				}
				case (int)Commands.UserLogin: {
					if(json.HasField("player_email")) {
						UserDetails user = new UserDetails();
						if(json.HasField("player_points")) {
							user.points = Convert.ToInt32(json.GetField("player_points").str);
						} else {
							Debug.Log("no response field player_points");
						}
						user.id = Convert.ToInt32(json.GetField("player_id").str);
						user.name = json.GetField("player_name").str;
						user.houseId = Convert.ToInt32(json.GetField("player_house_id").str);
						user.email = json.GetField("player_email").str;
						this.OnUserLoginSuccess(this, user);
					} else {
						this.OnUserLoginFailed(this, new BackendRequestFailedArgs(-1, "User Login Failed: user not found or incorrect password"));
					}
					break;
				}
				case (int)Commands.HouseCreate: {
					if(json.HasField("id")) {
						HouseDetails details = new HouseDetails();
						if(json.HasField("id")) {
							details.id = Convert.ToInt32(json.GetField("id").str);
						}
						if(json.HasField("name")) {
							details.name = json.GetField("name").str;
						}
						if(json.HasField("description")) {
							details.description = json.GetField("description").str;
						}
						this.OnHouseCreateSuccess(this, details);
					} else {
						this.OnHouseCreateFailed(this, new BackendRequestFailedArgs(-1, "House Create Failed: another house with the same name already exists"));
					}
					break;
				}
				case (int)Commands.HouseFind: {
					if(json.type == JSONObject.Type.ARRAY) {
						HouseListingDetails details = new HouseListingDetails();
						foreach(JSONObject j in json.list) {
							HouseDetails house = new HouseDetails();
							house.name = j.GetField("house_name").str;
							house.id = Convert.ToInt32(j.GetField("house_id").str);
							house.description = j.GetField("house_description").str;
							details.list.Add(house);
						}
						this.OnHouseFindSuccess(this, details);
					} else {
						this.OnHouseFindFailed(this, new BackendRequestFailedArgs(-1, "House Find Failed: No house with that name or description was found"));
					}
					break;
				}
				case (int)Commands.HouseJoin: {
//					print("Join House -> Response");
					if(json.HasField("passed")) {
						if(json.GetField("passed").b) {
							this.OnHouseJoinSuccess(this, new EventArgs());
							break;
						}
					} else {
						this.OnHouseJoinFailed(this, new BackendRequestFailedArgs(-1, "House Join Failed: invalid user or house id"));
					}					
					break;	
				}
				case (int)Commands.HouseListUsers: {
					if(json.type == JSONObject.Type.ARRAY) {
						UserListingDetails users = new UserListingDetails();
						foreach(JSONObject j in json.list) {
							UserDetails user = new UserDetails();
							user.name = j.GetField("player_name").str;
							user.id = Convert.ToInt32(j.GetField("player_id").str);
							if(j.GetField("player_house_id").type == JSONObject.Type.NULL) {
								user.houseId = -1;
							} else {
								user.houseId = Convert.ToInt32(j.GetField("player_house_id").str);
							}
							user.email = j.GetField("player_email").str;
							user.points = Convert.ToInt32(j.GetField("player_points").str);						
							users.list.Add(user);
						}
						this.OnUserListingForHouseReceived(this, users);
					} else {
						//no error event, only users of a house can request these details, 
						//so the house will ALWAYS return a list of at least 1 user
					}
					break;
				}
				case (int)Commands.HouseListAll: {
					if(json.type == JSONObject.Type.ARRAY) {
						HouseListingDetails details = new HouseListingDetails();
						foreach(JSONObject j in json.list) {
							HouseDetails house = new HouseDetails();
							house.name = j.GetField("house_name").str;
							house.id = Convert.ToInt32(j.GetField("house_id").str);
							house.description = j.GetField("house_description").str;
							details.list.Add(house);
						}
						this.OnHouseListingReceived(this, details);
					} else {
						//no error event, there will always be houses, even if only our testing houses
						Debug.Log("find house commands didn't return an array??"); //this should never happen
					}
					break;
				}
				default: {
					Debug.Log("Unknown or un-implemented command");
					break;
				}
			}
		} else {
			Debug.Log("www error: " + www.error);
		}
	}	
	public delegate void onUserLoginSuccess(Backend b, UserDetails user);
	public delegate void onUserLoginFailed(Backend b, BackendRequestFailedArgs args);
	public delegate void onUserCreateSuccess(Backend b, UserDetails user);
	public delegate void onUserCreateFailed(Backend b, BackendRequestFailedArgs args);
	public delegate void onHouseJoinSuccess(Backend b, EventArgs e);
	public delegate void onHouseJoinFailed(Backend b, BackendRequestFailedArgs args);
	public delegate void onHouseCreateSuccess(Backend b, HouseDetails house);
	public delegate void onHouseCreateFailed(Backend b, BackendRequestFailedArgs args);
	public delegate void onHouseFindSuccess(Backend b, HouseListingDetails houselisting);
	public delegate void onHouseFindFailed(Backend b, BackendRequestFailedArgs args);
	public delegate void onHouseListingReceived(Backend b, HouseListingDetails houselisting);
	public delegate void onUserListingForHouseReceived(Backend b, UserListingDetails userlisting);
	
	
}
