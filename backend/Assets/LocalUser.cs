using UnityEngine;
using System.Collections;

/** LocalUser just needs to sit inside of your unity project, 
	it gives you access to the LocalPlayer data in any unity C# script */

public class LocalUser : MonoBehaviour {
	public static int id;
	public static string displayName;
	public static string email;
	public static int houseId;
	public static int points;
	public static string passwordHashed;
}
