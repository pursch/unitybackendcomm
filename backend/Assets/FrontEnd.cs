using UnityEngine;
using System.Collections;

/** this file consists of Demo code */

public class FrontEnd : MonoBehaviour {
	public Backend backEndObject;
	
	/** the key to using the C# event system is to understand it's two parts:
	 - Request
	 - Responce
	
	Requests can be made anywhere where the BackEnd class is instantiated, but to retrieve the results
	you need to register an event handler that is triggered when the data is received back from the server

	You need to make sure that you have an eventlistener registered before you make your Request or
	you will not be able to retrieve the Responce from the server

	There are a number of Requests/Commands that you can use:

	UserCreate
	UserLogin
	UserFetchListingForHouse (get all users registered to a house)
	HouseCreate (create a house, user that creates it is automatically assigned to the newly created house)
	HouseJoin (leave any house the user is in, and assign them to a different one)
	HouseFind (find a house by name or discription, or both)
	HouseListAll (list all houses that exist on the server)


	In order to listen for events, you need to register for them. 
	Below we assume the backEndObject is available here, and register a listener for the OnUserCreatedSuccess event, 
	so that we can ask the server to create a new user for us, and process the results in the listener. 
	You can register any number of event listeners.

	(hint: if you type backEndObject.OnUserLoginSuccess += and autocomplete, it'll insert an anonymous delegate for you) */
	
	void Start () {
		if(backEndObject) {
			//register the listener first, to deal with the server responce
			backEndObject.OnUserLoginSuccess += delegate(Backend b, UserDetails user) {
				LocalUser.displayName = user.name;
				LocalUser.id = user.id;
				LocalUser.points = user.points;
				LocalUser.email = user.email;
				LocalUser.houseId = user.houseId;
				LocalUser.passwordHashed = user.passwordHashed;
			};
			//then make the request to the server
			backEndObject.userLogin("ruud", "balls", false);

			/** in order for you to be able to make use of user data, you will need to ensure that you make your requests that
				relate to the user after your OnUserLoginSuccess is called. the easiest way to do this.. is to do this inside of 
				your OnUserLoginSuccess handler/delegate/listener */

			/** similarly, if you don't have a user account yet, then you will need to wait with logging in until 
				an account has been created. so it's a good idea to just place any requests you want to make after the account
				is created, inside of the OnUserCreateSuccess handler/delegate/listener. In this case, the user is logged in once
	 			the account is created */				
			backEndObject.OnUserCreateSuccess += delegate(Backend b, UserDetails user) {
				Debug.Log("User created with name: " + user.name + " passwd: " + user.passwordHashed + " id: " + user.id + " email:" + user.email);
				b.userLogin(user.name, user.passwordHashed, true);
			};
			
			/** register a listener to OnHouseCreateSuccess when you want to handle the results of sending a create house request */	
			backEndObject.OnHouseCreateSuccess += delegate(Backend b, HouseDetails house) {
				Debug.Log("House created with name: "+ house.name +" descr: "+ house.description +" id: " + house.id);
			};
			
			/** more examples below */
			//backEndObject.houseCreate("Kame House", "home of master Roshi", LocalUser.id);
			
			backEndObject.OnHouseFindSuccess += delegate(Backend b, HouseListingDetails houselisting) {
				Debug.Log("Looked for houses, here are the results:");
				foreach(HouseDetails house in houselisting.list) {
					Debug.Log("House with name: " + house.name + " descr: " + house.description + " id: " + house.id);
				}
				Debug.Log("joining the first result..");
			};
//			backEndObject.houseFind("mancave", "");
			backEndObject.OnHouseJoinSuccess += delegate {
				Debug.Log("Joined the house OK!");
			};
			backEndObject.OnUserListingForHouseReceived += delegate(Backend b, UserListingDetails userlisting) {
				Debug.Log("Retreived all users for this house:");
				foreach(UserDetails user in userlisting.list) {
					Debug.Log("name: " + user.name + " id: " + user.id + " houseId: " + user.houseId + " email: " + user.email);	
				}
			};
//			backEndObject.userFetchListingForHouseWithId(LocalUser.houseId);
			backEndObject.OnHouseListingReceived += delegate(Backend b, HouseListingDetails houselisting) {
				Debug.Log("Complete house listing retrieved:");
				foreach(HouseDetails house in houselisting.list) {
					Debug.Log("House Name: " + house.name + " id: " + house.id + " descr: " + house.description);	
				}
			};
			backEndObject.houseFetchListing();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
